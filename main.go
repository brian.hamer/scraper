package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"

	"github.com/gocolly/colly"
)

type Seller struct {
	Name   string
	Amount int
}

func main() {
	response := Scraper()
	saveTxt(response)
}

// Scraper
func Scraper() []Seller {

	URL := "https://celulares.mercadolibre.com.co/_BestSellers_YES"

	if URL == "" {
		log.Println("missing URL argument")
		return nil
	}

	log.Println("visiting", URL)

	c := colly.NewCollector()

	var pageLinks []string
	c.OnHTML(".andes-pagination__button", func(e *colly.HTMLElement) {
		pageLinks = append(pageLinks, e.ChildAttr("a", "href"))

	})
	c.Visit(URL)

	c1 := colly.NewCollector()
	var productlinks []string
	c1.OnHTML(".ui-search-layout__item", func(e *colly.HTMLElement) {
		productlinks = append(productlinks, e.ChildAttr("a", "href"))

	})

	for _, page := range pageLinks {
		c1.Visit(page)
	}

	c2 := colly.NewCollector()
	responseSellers := make(map[string]int)
	seller := ""
	c2.OnHTML(".layout-description-wrapper", func(e *colly.HTMLElement) {
		re := regexp.MustCompile("[0-9]+")
		sold, _ := strconv.Atoi(strings.Join(re.FindAllString(strings.Replace(e.ChildText(".item-conditions"), "\t", "", -1), -1), " "))

		c3 := colly.NewCollector()
		c3.OnHTML(".official-store", func(e *colly.HTMLElement) {
			seller = e.ChildText("h3")
		})
		c3.OnHTML(".store-info-title", func(e *colly.HTMLElement) {
			seller = e.ChildText("h3")
		})

		responseSellers[seller] += sold
		fmt.Println("sellers", responseSellers)

		c3.Visit(e.ChildAttr("#seller-view-more-link", "href"))

	})

	for _, product := range productlinks {
		c2.Visit(product)
	}

	return Best100Sellers(responseSellers)
}

func Best100Sellers(responseSellers map[string]int) []Seller {

	bs := []Seller{}
	for k, v := range responseSellers {
		bs = append(bs, Seller{Name: k, Amount: v})
	}

	sort.Slice(bs, func(i, j int) bool {
		return bs[i].Amount > bs[j].Amount
	})

	return bs[:99]
}

func saveTxt(toText []Seller) {
	file, err := os.OpenFile("sellers.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)

	if err != nil {
		log.Fatalf("failed creating file: %s", err)
	}

	datawriter := bufio.NewWriter(file)

	for _, data := range toText {
		_, _ = datawriter.WriteString("Seller" + data.Name + ", Amount" + string(data.Amount))
	}
}
