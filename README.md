# GO CHALLENGE

## Descripción

Dada la siguiente dirección: https://celulares.mercadolibre.com.co/_BestSellers_YES. La consigna es obtener los ​100 vendedores con más operaciones​ para la
categoría indicada en la URL anterior.

## APP

Para correr la app debemos pararnos en el path del proyecto y correr go run main.go y generará un archivo txt con los 100 vendedores que mas vendieron dentro del mismo.

## Resultados

- MBP-de-Brian:scraper brianhamer$ go run main.go 
- 2020/10/28 23:15:42 visiting https://celulares.mercadolibre.com.co/_BestSellers_YES
- sellers map[:776 Realme Tienda oficial:1659]
- sellers map[:776 CELUMOVIL STORE:2140 Realme Tienda oficial:1659]
- sellers map[:776 CELUMOVIL STORE:2140 MEGATIENDAVIRTUAL77:1170 Realme Tienda oficial:1659]
- sellers map[:776 CELUMOVIL STORE:3442 MEGATIENDAVIRTUAL77:1170 Realme Tienda oficial:1659]
- sellers map[:776 CELUMOVIL STORE:3442 MEGATIENDAVIRTUAL77:1170 Motorola Tienda oficial:2834 Realme Tienda oficial:1659]
- sellers map[:776 CELUMOVIL STORE:3459 MEGATIENDAVIRTUAL77:1170 Motorola Tienda oficial:2834 Realme Tienda oficial:1659]
- sellers map[:776 CELUMOVIL STORE:3459 MEGATIENDAVIRTUAL77:1170 Motorola Tienda oficial:2834 Realme Tienda oficial:1659 Samsung Tienda oficial:1302]
- sellers map[:776 CELUMOVIL STORE:3459 MEGATIENDAVIRTUAL77:1170 Motorola Tienda oficial:3118 Realme Tienda oficial:1659 Samsung Tienda oficial:1302]
- sellers map[:776 CELUMOVIL STORE:3459 MEGATIENDAVIRTUAL77:1170 Motorola Tienda oficial:3769 Realme Tienda oficial:1659 Samsung Tienda oficial:1302]
- sellers map[:776 CELUMOVIL STORE:4243 MEGATIENDAVIRTUAL77:1170 Motorola Tienda oficial:3769 Realme Tienda oficial:1659 Samsung Tienda oficial:1302]
- sellers map[:776 CELUMOVIL STORE:5526 MEGATIENDAVIRTUAL77:1170 Motorola Tienda oficial:3769 Realme Tienda oficial:1659 Samsung Tienda oficial:1302]

## DB - Relacional - como futura implementación.

Se podria levantar con docker una DB para guardar el seller con su cantidad de ventas. A medida que el scraper encuentra un producto almacena en la tabla el vendedor y en caso de ya existir debería hacer un update sumandole las ventas con que hizo con ese producto nuevo.

## Go Routines - Faltante

Dada la gran tardanza que generá recorrer todas las páginas, publicaciones y yendo a buscar por cada una de ellas la cantidad vendida y el nombre del vendedor es indispensable la utilización de ellas. La idea es la de paralelizar e ir disparando rutinas por todas las páginas y que cada rutina vaya a buscar las publicaciones para obtener los datos. Se intento la implentación de ellas pero no pude obtener resultados ya que se me cortaba antes de tiempo y no lograba tener resultados. Quizás por la utilización de multiples rutinas atacando la pagina de Mercado Libre o algun error propio del codigo. 

Se intento lo siguiente:

- c.OnHTML(".andes-pagination__button", func(e *colly.HTMLElement) { //Encuentro una Pagina
-     go visitPages(e.ChildAttr("a", "href"))             //Disparo una GO Rutine con esa URL y que vaya a buscar productos con una Pagina determinada
- })
- c0.Visit(page)


- c1.OnHTML(".ui-search-layout__item", func(e *colly.HTMLElement) {    //Encuentro los productos
-     		go createSellerAndPrint(e.ChildAttr("a", "href"))        //Dispara una GO Rutina con el producto para ir a buscar los Datos
- })
- c1.Visit(page)    //Visito la pagina

Luego se debe obtener los datos del producto y utilizar el channel para guardar los datos en el responseSeller y liberar el recurso para que las demas GO Rutines la utilicen.

- c2.OnHTML(".layout-description-wrapper", func(e *colly.HTMLElement) {
-     sold, _ := e.ChildText(".item-conditions")
-     c3.OnHTML(".official-store", func(e *colly.HTMLElement) {
-     seller = e.ChildText("h3")
- })

- responseSellers[seller] += sold